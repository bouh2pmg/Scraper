#!/usr/bin/python3
from threading import Thread
from django.core.management.base import BaseCommand, CommandError
from scraper.management.commands.intra_api import IntraAPI

from scraper.models import Instance, Student_module, Student

MAX_THREAD = 50


def start_join_tasks(tasks):
    for task in tasks:
        task.start()
    for task in tasks:
        task.join()


class GetStudentInstances(Thread):
    def __init__(self, intra, instance, instances):
        Thread.__init__(self)
        self.intra = intra
        self.code_instance = instance['code_instance']
        self.code_module = instance['module_id']
        self.nb_instances = len(instances)
        self.index = instances.index(instance)
        self.student_instances = []

    def run(self):
        print("START\t[%d / %d]\t%s\t\t%s" % (self.index, self.nb_instances, self.code_instance, self.code_module))
        students = self.intra.make_call_api('/module/2018/%s/%s/registered' % (self.code_module, self.code_instance), {}, self.intra.intra_login)
        for student in students:
            try:
                student_db = Student.objects.get(login=student['login'])
                try:
                    elm = Student_module.objects.get(student__login=student['login'],
                                                     module__code_instance=self.code_instance,
                                                     module__module__code_module=self.code_module)
                except:
                    elm = Student_module(student=student_db,
                                         module__code_instance=self.code_instance,
                                         module__module__code_module=self.code_module)
                elm.grade = student['grade']
                elm.credits = student['credits']
                if elm.id:
                    elm.save()
                else:
                    self.student_instances.append(elm)
            except:
                pass
        Student_module.objects.bulk_create(self.student_instances)
        print("FINISH\t[%d / %d]\t%s\t\t%s" % (self.index, self.nb_instances, self.code_instance, self.code_module))


def get_grades(intra):
    tasks = []
    instances = list(Instance.objects.filter(year=2018).values())
    for instance in instances:
        tasks.append(GetStudentInstances(intra, instance, instances))
        if len(tasks) >= MAX_THREAD:
            start_join_tasks(tasks)
            tasks = []
    start_join_tasks(tasks)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('authlink', type=str)

    def handle(self, *args, **options):
        if "authlink" not in options:
            raise CommandError('you must provide intra authlink')
        intra = IntraAPI(options['authlink'])
        get_grades(intra)
