import logging
from django.core.management.base import BaseCommand, CommandError
from scraper.management.commands.intra_api import IntraAPI
from scraper.management.commands.utils import get_current_year
from scraper.models import *
from os.path import join
from scraper.settings import LOG_PATH

logging.basicConfig(filename=join(LOG_PATH, 'log'), level=logging.INFO, format='%(asctime)-15s;%(message)s')

elements = []


def get_modules(intra):
    year = get_current_year()
    params = {
        'year': year,
        'mode': 'template'
    }
    data = intra.make_call_api('/course/filter', params, intra.intra_login)
    for module in data:
        try:
            if int(module['semester']) >= 0 and module['code'][:2] in ("B-", "T-", "G-", "M-") and int(module['scolaryear']) == year:
                try:
                    elm = Module.objects.get(code_module=module['code'])
                    elm.name = module['title']
                    elm.semester = module['semester']
                    elm.save()
                except:
                    elm = Module.objects.create(code_module=module['code'], name=module['title'], semester=module['semester'])
                elements.append(elm.code_module)
        except:
            logging.error("{} db request failed".format(module['code']))


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('authlink', type=str)

    def handle(self, *args, **options):
        if "authlink" not in options:
            raise CommandError('you must provide intra authlink')
        intra = IntraAPI(options['authlink'])
        get_modules(intra)
        Module.objects.exclude(code_module__in=elements).delete()

