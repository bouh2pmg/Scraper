import logging
from time import sleep
from threading import Thread
from django import db

from django.core.management.base import BaseCommand, CommandError
from scraper.management.commands.intra_api import IntraAPI
from scraper.management.commands.utils import get_current_year, get_promotion_year, check_cursus, start_join_tasks
from scraper.models import *
from os.path import join
from scraper.settings import LOG_PATH

logging.basicConfig(filename=join(LOG_PATH, 'log'), level=logging.INFO, format='%(asctime)-15s;%(message)s')

nb_thread = 0
lock = False
max_thread = 50
elements = []


class GetPromotions(Thread):
    def __init__(self, intra, city, year):
        Thread.__init__(self)
        self.intra = intra
        self.city = city
        self.year = year

    def get_cursus(self, intra, city_code, year):
        params = {
            'location': city_code,
            'year': year,
            'active': 'true'
        }
        data = intra.make_call_api('/course', params)
        return data

    def get_promotion(self, intra, city_code, year, course=None):
        params = {
            'location': city_code,
            'year': year,
            'active': 'true'
        }
        if course:
            params['course'] = course
        data = intra.make_call_api('/promo', params)
        return data

    def run(self):
        try:
            global nb_thread, elements
            while nb_thread > max_thread:
                sleep(1)
            print("START %s" % self.city.code)
            nb_thread += 1
            courses = self.get_cursus(self.intra, self.city.code, self.year)
            for cursus in courses:
                if check_cursus(cursus['code']):
                    for promo in self.get_promotion(self.intra, self.city.code, self.year, cursus['code']):
                        promotion_year = get_promotion_year(promo['promo'], self.year)
                        global lock
                        while lock:
                            sleep(0.2)
                        lock = True
                        try:
                            elm = Promo.objects.get(city=self.city, promotion_year=promotion_year, cursus=cursus['code'])
                        except:
                            elm = Promo.objects.create(city=self.city, promotion_year=promotion_year, cursus=cursus['code'])
                        elements.append(elm.id)
                        db.connections.close_all()
                        lock = False
            nb_thread -= 1
            print("FINISH %s" % self.city.code)
        except:
            logging.error("get promotions of {} of {} failed".format(self.city, self.year))


def get_promotions(intra):
    year = get_current_year()
    tasks = []
    for city in City.objects.all():
        tasks.append(GetPromotions(intra, city, year))
        if len(tasks) >= max_thread:
            start_join_tasks(tasks)
            tasks = []
    start_join_tasks(tasks)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('authlink', type=str)

    def handle(self, *args, **options):
        if "authlink" not in options:
            raise CommandError('you must provide intra authlink')
        intra = IntraAPI(options['authlink'])
        get_promotions(intra)
        Promo.objects.exclude(id__in=elements).delete()
