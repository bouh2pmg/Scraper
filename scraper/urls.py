from django.conf.urls import url
from django.contrib import admin
from django.urls import include
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view
from rest_framework.authtoken import views
from django.db.models import Q
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from scraper.views import *
from scraper.ViewSet import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from scraper.models import *
from django.db.models import Avg

router = routers.DefaultRouter()
router.register(r'cities', CityViewSet)
router.register(r'promos', PromoViewSet)
router.register(r'students', StudentViewSet)
router.register(r'modules', ModuleViewSet)
router.register(r'instances', InstanceViewSet)
router.register(r'students_instance', StudentsInstancesViewSet)
router.register(r'students_roadblocks', RoadblockViewSet)
router.register(r'grade_history', GradeHistoryViewSet)
router.register(r'tepitech', TepitechViewSet)


class get_roadblock_stats(APIView):
    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication)
    permission_classes = (permissions.IsAuthenticated,)

    @method_decorator(cache_page(0))
    def get(self, req, format=None):
        cities_roadblocks = {
            'labels': [],
            'hard': [],
            'innov': [],
            'soft': []
        }
        cities = {promo['city__code']: promo['city__name'] for promo in Promo.objects.filter(promotion_year=timezone.now().year + 3, cursus__in=['bachelor/classic', 'bachelor/tek1ed', 'bachelor/tek2ed', 'bachelor/tek3s', 'master/classic', 'master/tek3si']).values('city__code', 'city__name').distinct()}
        city_list = req.GET.getlist('cities[]')
        if not city_list:
            city_list = cities.keys()
        for city in city_list:
            cities_roadblocks['labels'].append(cities[city])
            students = Student_roadblock.objects.filter(student__promo__city__code=city)
            if 'danger' in req.GET and req.GET['danger']:
                students = students.filter(Q(innovation__lt=15) | Q(expertise__lt=15) | Q(softskills__lt=5))
            avg = students.aggregate(Avg('innovation'), Avg('expertise'), Avg('softskills'))
            cities_roadblocks['hard'].append(int(avg['expertise__avg']) if avg['expertise__avg'] else 0)
            cities_roadblocks['innov'].append(int(avg['innovation__avg']) if avg['innovation__avg'] else 0)
            cities_roadblocks['soft'].append(int(avg['softskills__avg']) if avg['softskills__avg'] else 0)
        if 'danger' in req.GET and req.GET['danger']:
            avg = Student_roadblock.objects.filter(Q(innovation__lt=15) | Q(expertise__lt=15) | Q(softskills__lt=5)).aggregate(Avg('innovation'), Avg('expertise'), Avg('softskills'))
        else:
            avg = Student_roadblock.objects.all().aggregate(Avg('innovation'), Avg('expertise'), Avg('softskills'))
        cities_roadblocks['labels'].append("National")
        cities_roadblocks['hard'].append(int(avg['expertise__avg']))
        cities_roadblocks['innov'].append(int(avg['innovation__avg']))
        cities_roadblocks['soft'].append(int(avg['softskills__avg']))
        return Response(cities_roadblocks)


urlpatterns = [
    url(r'^docs/$', get_swagger_view(title='API')),
    url(r'^api/get_token$', views.obtain_auth_token),
    url(r'^api/', include(router.urls)),
    url(r'^admin/', admin.site.urls),

    url(r'^api/get_roadblock_stats$', get_roadblock_stats.as_view()),
    url(r'^api/get_module_stats/(?P<year>[\d]{4})/(?P<code_module>[\w\d-]+)$', get_module_stats),
    url(r'^api/get_module_alerts/$', get_module_alerts),
    url(r'^api/get_tepitech_alerts/$', get_tepitech_alerts),
]
