import logging
from threading import Thread
from time import sleep

from django import db
from django.core.management.base import BaseCommand, CommandError
from scraper.management.commands.intra_api import IntraAPI
from os.path import join
from scraper.management.commands.utils import get_current_year, start_join_tasks
from scraper.models import Module, Student_module, GradeHistory
from scraper.settings import LOG_PATH

logging.basicConfig(filename=join(LOG_PATH, 'log'), level=logging.INFO, format='%(asctime)-15s;%(message)s')

nb_thread = 0
max_thread = 200
lock = False


class GetStudentInstances(Thread):
    def __init__(self, intra, module, instance):
        Thread.__init__(self)
        self.intra = intra
        self.module = module
        self.instance = instance

    def run(self):
        global nb_thread, lock
        while nb_thread > max_thread:
            sleep(1)
        #print("START %s:%s:%s" % (self.module.code_module, self.instance['scolaryear'], self.instance['code']))
        nb_thread += 1
        students = self.intra.make_call_api('/module/%s/%s/%s/registered' % (self.instance['scolaryear'], self.module.code_module, self.instance['code']),
                                            {'format': 'json'}, self.intra.intra_login)
        if not students:
            nb_thread -= 1
            return
        grades = {}
        for grade in Student_module.GRADES:
            grades[grade[0][0]] = 0
        for student in students:
            grades[student['grade'][0]] += 1
        while lock:
            sleep(0.2)
        lock = True
        try:
            grade = GradeHistory.objects.get(module=self.module, instance=self.instance['code'], year=self.instance['scolaryear'])
            grade.NA = grades['-']
            grade.A = grades['A']
            grade.B = grades['B']
            grade.C = grades['C']
            grade.D = grades['D']
            grade.E = grades['E']
        except:
            grade = GradeHistory(module=self.module, instance=self.instance['code'], year=self.instance['scolaryear'], NA=grades['-'],
                                 A=grades['A'], B=grades['B'], C=grades['C'], D=grades['D'], E=grades['E'])
        grade.save()
        db.connections.close_all()
        lock = False
        nb_thread -= 1
        #print("FINISH %s:%s:%s" % (self.module.code_module, self.instance['scolaryear'], self.instance['code']))


def get_grades(intra):
    tasks = []
    year = get_current_year()
    for module in Module.objects.all():
        data = intra.make_call_api('/module/%d/%s' % (year, module.code_module), {'format': 'json'}, intra.intra_login)
        for instance in data['instances']:
            tasks.append(GetStudentInstances(intra, module, instance))
            if len(tasks) >= max_thread:
                start_join_tasks(tasks)
                tasks = []
    start_join_tasks(tasks)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('authlink', type=str)

    def handle(self, *args, **options):
        if "authlink" not in options:
            raise CommandError('you must provide intra authlink')
        intra = IntraAPI(options['authlink'])
        get_grades(intra)
#        for elm in GradeHistory.objects.all():
#            try:
#                Instance.objects.get(module=elm.module, code_instance=elm.instance, year=elm.year)
#            except:
#                elm.delete()
