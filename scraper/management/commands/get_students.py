import logging
from time import sleep
from threading import Thread
from django.core.management.base import BaseCommand, CommandError
from scraper.management.commands.intra_api import IntraAPI
from scraper.management.commands.utils import get_current_year, check_cursus, start_join_tasks
from scraper.models import *
from django import db
from os.path import join
from scraper.settings import LOG_PATH
logging.basicConfig(filename=join(LOG_PATH, 'log'), level=logging.INFO, format='%(asctime)-15s;%(message)s')


nb_thread = 0
max_thread = 100
lock = False
elements = []


def get_city_students(intra, city, year):
    params = {
        'nolimit': True,
        'location': city.code,
        'year': year,
        'active': 'true',
    }
    students = intra.make_call_api('/user', params)
    return students


class GetStudent(Thread):
    def __init__(self, intra, student, city):
        Thread.__init__(self)
        self.intra = intra
        self.student = student
        self.city = city

    def run(self):
        try:
            global nb_thread, elements
            while nb_thread > max_thread:
                sleep(1)
            print("START %s" % self.student['login'])
            nb_thread += 1
            info = self.intra.make_call_api('/user/' + self.student['login'], {}, self.intra.intra_login)
            if check_cursus(info['course_code']):
                promo = Promo.objects.get(city=self.city, promotion_year=info['promo'], cursus=info['course_code'])
                global lock
                while lock:
                    sleep(0.2)
                lock = True
                try:
                    elm = Student.objects.filter(login=self.student['login']).first()
                    elm.firstname = info['firstname']
                    elm.lastname = info['lastname']
                    elm.promo = promo
                    elm.credit_acquired = info['credits']
                    elm.save()
                except:
                    elm = Student.objects.create(login=self.student['login'], firstname=info['firstname'],
                                                 lastname=info['lastname'], promo=promo, credit_acquired=info['credits'])
                elements.append(elm.login)
                db.connections.close_all()
                lock = False
            nb_thread -= 1
            print("FINISH %s" % self.student['login'])
        except:
            logging.error("get {} ({} {}) failed".format(self.student.login, self.student.promo.promotion_year, self.student.promo.city.code))


def get_students(intra):
    year = get_current_year()
    tasks = []
    for city in City.objects.all():
        for student in get_city_students(intra, city, year)['items']:
            tasks.append(GetStudent(intra, student, city))
            if len(tasks) >= max_thread * 2:
                start_join_tasks(tasks)
                tasks = []
    start_join_tasks(tasks)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('authlink', type=str)

    def handle(self, *args, **options):
        if "authlink" not in options:
            raise CommandError('you must provide intra authlink')
        intra = IntraAPI(options['authlink'])
        get_students(intra)
        Student.objects.exclude(login__in=elements).delete()
