roadblocks = {
    'innovation': [
        'B-INN-500', 'B-EIP-500', 'B-MOO-500', 'B-MOO-501', 'B-MOO-502', 'B-PRO-500'
    ],
    'expertise': [
        'B-CPP-500', 'B-CPP-501', 'B-CPP-510', 'B-DEV-500', 'B-DEV-501', 'B-DEV-510', 'B-FUN-500', 'B-FUN-501', 'B-FUN-510', 'B-AIA-500', 'B-MAT-500', 'B-SEC-500', 'B-NSA-500', 'G-SEC-010'
    ],
    'softskills': [
        'B-FRE-501', 'B-ANG-001', 'B-PCP-000', 'G-COC-001', 'G-FRE-010', 'G-EPI-000', 'G-EPI-010', 'G-EPI-004', 'G-CUS-000', 'G-CUS-002'
    ]
}
