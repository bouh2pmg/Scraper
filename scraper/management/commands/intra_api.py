#!/usr/bin/python3
import logging
from os.path import join
from scraper.management.commands.my_requests import RequestRetry
from scraper.settings import LOG_PATH
logging.basicConfig(filename=join(LOG_PATH, 'log'), level=logging.INFO, format='%(asctime)-15s;%(message)s')


class IntraAPI:
    intra_url = "https://intra.epitech.eu{0}"
    intra_login = None
    user_filter = None

    def __init__(self, authlink):
        self.intra_login = self.intra_url.format('/' + authlink + '{0}')
        self.user_filter = self.intra_login.format('/user/filter{0}')

    def make_call_api(self, call, params={}, url=None):
        if not url:
            url = self.user_filter
        params['format'] = 'json'
        try:
            r = RequestRetry.get(url.format(call), params=params)
            if r.status_code not in (200, 201):
                raise Exception(r.status_code)
            return r.json()
        except:
            logging.error("intra call '{}' failed with params {}".format(url.format(call), params))
            return None
