from django.http import JsonResponse
from django.utils import timezone
from scraper.models import *
from scraper.management.commands.utils import get_current_year
from django.db.models import Sum
from scraper.management.commands.utils import get_current_year


def get_module_stats(req, year, code_module):
    percent = 'percent' in req.GET and int(req.GET['percent']) != 0
    instances = GradeHistory.objects.filter(year=year, module__code_module=code_module).order_by('instance')
    national = instances.aggregate(A=Sum('A'), B=Sum('B'), C=Sum('C'), D=Sum('D'), E=Sum('E'), NA=Sum('NA'))
    if 'instances[]' in	req.GET	and len(req.GET.getlist('instances[]')):
        instances = instances.filter(instance__in=req.GET.getlist('instances[]'))
    datas = {'labels': list(instances.values_list('instance', flat=True))}
    grades = {'A': 'A', 'B': 'B', 'C': 'C', 'D': 'D', 'Echec': 'E', '-': 'NA'}
    for grade in grades:
        datas[grade] = []
    for instance in instances:
        for grade in grades:
            datas[grade].append(getattr(instance, grades[grade]))
    if percent:
        for idx in range(len(datas['labels'])):
            total = 0
            for grade in grades:
                total += datas[grade][idx]
            for grade in grades:
                datas[grade][idx] = round(datas[grade][idx] / total * 100, 2)
        total = sum(national.values())
        for grade in grades:
            datas[grade].append(round(national[grades[grade]] / total * 100, 2))
    datas['labels'].append('National')
    return JsonResponse(datas, safe=True)


def get_current_year():
    return timezone.now().year - (1 if timezone.now().month in range(1, 9) else 0)


def get_module_alerts(req):
    city_insts = {}
    normal = {}
    for city in City.objects.all():
        instances = Instance.objects.filter(year=get_current_year(), end__lt=timezone.now(), code_instance__startswith=city.code.split('/')[1])
        city_insts[city.name] = 0
        normal[city.name] = 0
        for instance in instances:
            city_insts[city.name] += GradeHistory.objects.filter(module=instance.module, instance=instance.code_instance, NA__gt=0, year=get_current_year()).values('module_id', 'instance', 'year').count()
            normal[city.name] += GradeHistory.objects.filter(module=instance.module, instance=instance.code_instance, year=get_current_year()).values('module_id', 'instance', 'year').count()
        total = city_insts[city.name] + normal[city.name]
        city_insts[city.name] = round(city_insts[city.name] / total * 100, 2) if total else 0
        normal[city.name] = round(normal[city.name] / total * 100, 2) if total else 0
    return JsonResponse({'alerts': city_insts, 'normal': normal})


def get_tepitech_alerts(req):
    alerts = {'labels': [], 'valide': [], '-50': [], '-100': [], '-150': [], 'alert': [], 'na': []}
    year = get_current_year()
    limits = {
        str(year + 3): 750,
        str(year + 4): 700,
        str(year + 5): 600,
    }
    for city in City.objects.all():
        alert = {'valide': 0, '-50': 0, '-100': 0, '-150': 0, 'alert': 0, 'na': 0}
        for promo_year in limits:
            students = TepitechScores.objects.filter(student__promo__city=city, best_tepitech=True, student__promo__promotion_year=promo_year)
            alert['valide'] += students.filter(score__gte=limits[promo_year]).count()
            alert['-50'] += students.filter(score__gte=limits[promo_year] - 50, score__lt=limits[promo_year]).count()
            alert['-100'] += students.filter(score__gte=limits[promo_year] - 100, score__lt=limits[promo_year] - 50).exclude(score=-1).count()
            alert['-150'] += students.filter(score__gte=limits[promo_year] - 150, score__lt=limits[promo_year] - 100).exclude(score=-1).count()
            alert['alert'] += students.filter(score__lt=limits[promo_year] - 150).exclude(score=-1).count()
            alert['na'] += students.filter(score=-1).count()
        alerts['labels'].append(city.name)
        for key in alert:
            try:
                alerts[key].append(round(alert[key] / sum(alert.values()) * 100, 2))
            except:
                alerts[key].append(0)
    return JsonResponse(alerts)
