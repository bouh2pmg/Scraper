from threading import Thread
from time import sleep
from django import db
from django.core.management.base import BaseCommand, CommandError
from scraper.management.commands.intra_api import IntraAPI
from scraper.management.commands.utils import get_current_year, start_join_tasks
from scraper.models import *
from scraper.settings import LOG_PATH
from os.path import join
import logging
from django.utils.timezone import get_current_timezone
from datetime import datetime

logging.basicConfig(filename=join(LOG_PATH, 'log'), level=logging.INFO, format='%(asctime)-15s;%(message)s')
nb_thread = 0
max_thread = 50
lock = False
db_req = 0


class GetTepitech(Thread):
    def __init__(self, intra, instance):
        Thread.__init__(self)
        self.intra = intra
        self.instance = instance

    def get_url_acti(self, acti):
        return '/module/%s/B-ANG-058/%s/%s' % (self.instance['scolaryear'], self.instance['code'], acti)

    def get_acti_info(self, actis, acti):
        data = self.intra.make_call_api(self.get_url_acti(acti), url=self.intra.intra_login)
        actis[acti] = data

    def run(self):
        try:
            global nb_thread, elements, db_req
            while nb_thread > max_thread:
                sleep(1)
            print("START Tepitech [%s] %s" % (self.instance['scolaryear'], self.instance['code']))
            nb_thread += 1
            data = self.intra.make_call_api('/module/%s/B-ANG-058/%s/notes' % (self.instance['scolaryear'], self.instance['code']), url=self.intra.intra_login)
            actis = {}
            if not data:
                print(self.instance)
            for student in data['listnotes']:
                tepitech_passed = False
                try:
                    std = Student.objects.get(login=student['login'])
                except:
                    continue
                for acti in list(student.keys())[1:]:
                    if student[acti] and ("tepitech" in data['columns'][acti]['help'].lower() or "toeic" in data['columns'][acti]['help'].lower()) and "self" not in data['columns'][acti]['help'].lower():
                        tepitech_passed = True
                        if acti not in actis:
                            self.get_acti_info(actis, acti)
                        date = get_current_timezone().localize(datetime.strptime(actis[acti]['end'], '%Y-%m-%d %H:%M:%S'))
                        elm = None
                        try:
                            elm = TepitechScores.objects.get(student=std, url_acti=self.get_url_acti(acti), date=date)
                            elm.score = student[acti]
                        except:
                            elm = TepitechScores(student=std, url_acti=self.get_url_acti(acti), date=date, score=student[acti])
                            if not TepitechScores.objects.filter(student=std, score__gte=student[acti]).count():
                                elm.best_tepitech = True
                            for old in TepitechScores.objects.filter(student=std, score__lt=student[acti], best_tepitech=True):
                                old.best_tepitech = False
                                old.save()
                        if int(student[acti]) <= 0:
                            continue
                        elm.save()
                TepitechScores.objects.filter(student=std, score=-1).delete()
                if not tepitech_passed:
                    TepitechScores(student=std, url_acti="no_tepitech", score=-1, best_tepitech=True).save()
            db.connections.close_all()
            nb_thread -= 1
            print("FINISH Tepitech [%s] %s" % (self.instance['scolaryear'], self.instance['code']))
        except:
            logging.error("{}".format(self.instance))


def get_tepitech(intra):
    tasks = []
    datas = intra.make_call_api('/module/2018/B-ANG-058', url=intra.intra_login)
    for instance in datas['instances']:
        if int(instance['scolaryear']) == get_current_year():
            tasks.append(GetTepitech(intra, instance))
            if len(tasks) >= max_thread:
                start_join_tasks(tasks)
                tasks = []
    start_join_tasks(tasks)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('authlink', type=str)

    def handle(self, *args, **options):
        if "authlink" not in options:
            raise CommandError('you must provide intra authlink')
        try:
            intra = IntraAPI(options['authlink'])
            get_tepitech(intra)
        except:
            logging.error("Scraping tepitech have failed")
