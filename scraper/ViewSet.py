from rest_framework import serializers, viewsets
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters

from scraper.models import *
import django_filters


class CitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = City
        fields = ('code', 'name')


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('code', 'name')
    ordering_fields = '__all__'


class PromoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Promo
        fields = ('id', 'city', 'promotion_year', 'cursus')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['city'] = CitySerializer(City.objects.get(code=data['city'])).data
        return data


class PromoViewSet(viewsets.ModelViewSet):
    queryset = Promo.objects.all()
    serializer_class = PromoSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('city', 'promotion_year', 'cursus')
    ordering_fields = '__all__'


class ModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = ('code_module', 'name')


class ModuleViewSet(viewsets.ModelViewSet):
    queryset = Module.objects.all()
    serializer_class = ModuleSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('code_module', 'name')
    ordering_fields = '__all__'


class InstanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instance
        fields = ('module', 'code_instance', 'year', 'start', 'end', 'roadblock')


class InstanceViewSet(viewsets.ModelViewSet):
    queryset = Instance.objects.all()
    serializer_class = InstanceSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('module', 'code_instance', 'year', 'start', 'end', 'roadblock')
    ordering_fields = '__all__'


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('login', 'firstname', 'lastname', 'promo', 'credit_acquired')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['promo'] = PromoSerializer(Promo.objects.get(id=data['promo'])).data
        return data


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('login', 'firstname', 'lastname', 'promo', 'credit_acquired')
    ordering_fields = '__all__'


class StudentsInstancesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student_module
        fields = ('student', 'module', 'credits', 'grade')


class StudentsInstancesViewSet(viewsets.ModelViewSet):
    queryset = Student_module.objects.all()
    serializer_class = StudentsInstancesSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('student', 'module', 'credits', 'grade')
    ordering_fields = '__all__'


class MiniPromoSerializer(serializers.ModelSerializer):
    city = CitySerializer(many=False, read_only=True)

    class Meta:
        model = Promo
        fields = ('city',)


class MiniStudentSerializer(serializers.ModelSerializer):
    promo = MiniPromoSerializer(many=False, read_only=True)

    class Meta:
        model = Student
        fields = ('login', 'promo')


class RoadblockSerializer(serializers.ModelSerializer):
    student = MiniStudentSerializer(many=False, read_only=True)

    class Meta:
        model = Student_roadblock
        fields = ('student', 'innovation', 'expertise', 'softskills')


class RoadblockFilter(django_filters.FilterSet):
    city = django_filters.rest_framework.CharFilter(field_name="student__promo__city__code", lookup_expr='exact')

    class Meta:
        model = Student_roadblock
        fields = ('student', 'city', 'innovation', 'expertise', 'softskills')


class RoadblockViewSet(viewsets.ModelViewSet):
    city = CitySerializer
    queryset = Student_roadblock.objects.all()
    serializer_class = RoadblockSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_class = RoadblockFilter
    ordering_fields = '__all__'


class GradeHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = GradeHistory
        fields = ('module', 'instance', 'year', 'A', 'B', 'C', 'D', 'E', 'NA')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        try:
            data['instance'] = InstanceSerializer(Instance.objects.get(module__code_module=data['module'], code_instance=data['instance'], year=data['year'])).data
        except:
            pass
        data['module'] = ModuleSerializer(Module.objects.get(code_module=data['module'])).data
        return data

from django.utils import timezone
from django.db.models.query import QuerySet

class GradeHistoryViewSet(viewsets.ModelViewSet):
    queryset = GradeHistory.objects.all()
    serializer_class = GradeHistorySerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('module', 'instance', 'year', 'A', 'B', 'C', 'D', 'E', 'NA')
    ordering_fields = '__all__'

    def get_queryset(self):
        queryset = self.queryset
        alert_only = self.request.query_params.get('alert_only', None) == "true"
        print(timezone.now())
        if alert_only:
            instances = list(Instance.objects.filter(end__lt=timezone.now()).distinct('module').values_list('module', flat=True))
            queryset = queryset.filter(module__in=instances, NA__gt=0)
        return queryset


class TepitechSerializer(serializers.ModelSerializer):
    student = StudentSerializer(many=False, read_only=True)

    class Meta:
        model = TepitechScores
        fields = ('student', 'url_acti', 'date', 'score', 'nb_tepitech')


class TepitechViewSet(viewsets.ModelViewSet):
    queryset = TepitechScores.objects.all()
    serializer_class = TepitechSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter)
    filter_fields = ('student', 'url_acti', 'date', 'score', 'best_tepitech')
    ordering_fields = '__all__'
