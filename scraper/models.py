from django.db import models
from django.utils import timezone


class City(models.Model):
    code = models.CharField(max_length=128, primary_key=True, unique=True)
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.code + " : " + self.name


class Promo(models.Model):
    city = models.ForeignKey('City', on_delete=models.CASCADE)
    promotion_year = models.IntegerField()
    cursus = models.CharField(max_length=128)

    def __str__(self):
        return str(self.promotion_year) + " : " + self.city.name + " : " + self.cursus


class Module(models.Model):
    code_module = models.CharField(max_length=128, primary_key=True, unique=True)
    name = models.CharField(max_length=128)
    semester = models.IntegerField(default=-1)

    def __str__(self):
        return self.code_module + " : " + self.name + " : " + str(self.semester)


class Instance(models.Model):
    module = models.ForeignKey('Module', on_delete=models.CASCADE)
    code_instance = models.CharField(max_length=128)
    year = models.IntegerField()
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    roadblock = models.BooleanField()

    def __str__(self):
        return self.module.code_module + " : " + self.code_instance + " : " + str(self.year)


class Student(models.Model):
    login = models.CharField(max_length=128, unique=True, primary_key=True)
    firstname = models.CharField(max_length=128)
    lastname = models.CharField(max_length=128)
    promo = models.ForeignKey('Promo', on_delete=models.CASCADE)
    credit_acquired = models.IntegerField()

    def __str__(self):
        return self.login + " : " + str(self.promo) + " : " + str(self.credit_acquired)


class Student_module(models.Model):
    student = models.ForeignKey('Student', on_delete=models.CASCADE)
    module = models.ForeignKey('Instance', on_delete=models.CASCADE)
    GRADES = (
        ('Acquis', 'Acquis'),
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
        ('Echec', 'Echec'),
        ('-', 'Not validated')
    )
    credits = models.IntegerField(default=0)
    grade = models.CharField(max_length=128, choices=GRADES, default='-')

    def __str__(self):
        return self.student.login + " : " + self.module.module.code_module + " : " + str(self.credits) + self.grade


class Student_roadblock(models.Model):
    student = models.OneToOneField('Student', on_delete=models.CASCADE, unique=True, primary_key=True)
    innovation = models.IntegerField()
    expertise = models.IntegerField()
    softskills = models.IntegerField()

    def __str__(self):
        return "%s : %d - %d - %d" % (self.student.login, self.innovation, self.expertise, self.softskills)


class GradeHistory(models.Model):
    module = models.ForeignKey('Module', on_delete=models.CASCADE)
    instance = models.CharField(max_length=10)
    year = models.IntegerField(default=timezone.now().year)
    A = models.IntegerField(default=0)
    B = models.IntegerField(default=0)
    C = models.IntegerField(default=0)
    D = models.IntegerField(default=0)
    E = models.IntegerField(default=0)
    NA = models.IntegerField(default=0)

    def __str__(self):
        return "[%d] %s : %s : {A: %d, B: %d, C: %d, D: %d, E: %d, NA: %d}" % (self.year, self.module.code_module, self.instance, self.A, self.B, self.C, self.D, self.E, self.NA)


class TepitechScores(models.Model):
    student = models.ForeignKey('Student', on_delete=models.CASCADE, null=True, blank=True)
    student_str = models.CharField(max_length=255, default="")
    url_acti = models.CharField(max_length=255)
    date = models.DateTimeField(null=True)
    score = models.IntegerField()
    best_tepitech = models.BooleanField(default=False)

    def nb_tepitech(self):
        return TepitechScores.objects.filter(student=self.student).exclude(date=None).count()

    def __str__(self):
        return "%s : %s : %d" % (self.student.login, str(self.date), self.score)

'''
class Weekly_log(models.Model):
    promo = models.ForeignKey('Promo', on_delete=models.CASCADE)
    date = models.DateTimeField()
    min = models.IntegerField()
    max = models.IntegerField()
    average = models.FloatField()

    def __str__(self):
        return self.promo.promotion_year.__str__() + " : " + str(self.average)


class Weekly_absences(models.Model):
    promo = models.ForeignKey('Promo', on_delete=models.CASCADE)
    date = models.DateTimeField()
    min = models.IntegerField()
    max = models.IntegerField()
    average = models.FloatField()

    def __str__(self):
        return self.promo.promotion_year.__str__() + " : " + str(self.average)
'''
