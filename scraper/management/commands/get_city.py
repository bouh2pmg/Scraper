from django.core.management.base import BaseCommand, CommandError

from scraper.management.commands.intra_api import IntraAPI
from scraper.models import *
from scraper.settings import LOG_PATH
from os.path import join
import logging

logging.basicConfig(filename=join(LOG_PATH, 'log'), level=logging.INFO, format='%(asctime)-15s;%(message)s')
elements = []


def get_city(intra):
    data = intra.make_call_api('/location', {'active': 'true'})
    for city in data:
        try:
            elm = City.objects.get(code=city['code'])
            elm.name = city['title']
            elm.save()
        except:
            elm = City.objects.create(code=city['code'], name=city['title'])
        elements.append(elm.code)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('authlink', type=str)

    def handle(self, *args, **options):
        if "authlink" not in options:
            raise CommandError('you must provide intra authlink')
        try:
            intra = IntraAPI(options['authlink'])
            get_city(intra)
            City.objects.exclude(code__in=elements).delete()
        except:
            logging.error("Scraping cities have failed")
