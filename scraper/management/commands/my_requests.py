import requests
from time import sleep
from urllib.parse import urlencode


class RequestRetry:
    @staticmethod
    def get(url, params={}, auth=None, retry=20):
        try:
            r = requests.get((url + "?{0}").format(urlencode(params)), auth=auth)
            if r.status_code == 403:
                raise requests.exceptions.ConnectionError
            return r
        except requests.exceptions.ConnectionError:
            if retry > 0:
                sleep(0.5)
                return RequestRetry.get(url, params=params, retry=retry-1)
            raise

    @staticmethod
    def request(method, url, headers={}, json={}, auth=None, retry=20):
        try:
            r = requests.request(method, url, headers=headers, json=json, auth=auth)
            if r.status_code == 403:
                raise requests.exceptions.ConnectionError
            return r
        except requests.exceptions.ConnectionError:
            if retry > 0:
                sleep(0.5)
                return RequestRetry.request(method, url, headers=headers, json=json, auth=auth, retry=retry-1)
            raise

    @staticmethod
    def delete(url, auth=None, retry=20):
        try:
            r = requests.delete(url, auth=auth)
            if r.status_code == 403:
                raise requests.exceptions.ConnectionError
            return r
        except requests.exceptions.ConnectionError:
            if retry > 0:
                sleep(0.5)
                return RequestRetry.delete(url, auth=auth, retry=retry-1)
            raise
