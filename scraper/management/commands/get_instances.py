from time import sleep
from threading import Thread
from django import db
from django.core.management.base import BaseCommand, CommandError
from scraper.management.commands.intra_api import IntraAPI
from scraper.management.commands.utils import get_current_year, start_join_tasks
from scraper.models import *
from datetime import datetime
import logging
from os.path import join
from scraper.settings import LOG_PATH

logging.basicConfig(filename=join(LOG_PATH, 'log'), level=logging.INFO, format='%(asctime)-15s;%(message)s')
nb_thread = 0
max_thread = 50
lock = False
elements = []


class GetInstance(Thread):
    def __init__(self, intra, module, year):
        Thread.__init__(self)
        self.intra = intra
        self.module = module
        self.year = year

    def run(self):
        try:
            global nb_thread, elements
            while nb_thread > max_thread:
                sleep(1)
            print("START %s" % self.module.code_module)
            nb_thread += 1
            data = self.intra.make_call_api('/module/%d/%s' % (self.year, self.module.code_module), url=self.intra.intra_login)
            for instance in data['instances']:
                try:
                    global lock
                    instance = self.intra.make_call_api('/module/%d/%s/%s' % (int(instance['scolaryear']), self.module.code_module, instance['code']), url=self.intra.intra_login)
                    start = datetime.strptime(instance['begin'], "%Y-%m-%d") if instance['begin'] else None
                    end = datetime.strptime(instance['end'], "%Y-%m-%d") if instance['end'] else None
                    try:
                        elm = Instance.objects.get(module=self.module, code_instance=instance['codeinstance'],
                                             year=int(instance['scolaryear']))
                        elm.start = start
                        elm.end = end
                        elm.save()
                    except:
                        elm = Instance.objects.create(module=self.module, code_instance=instance['codeinstance'],
                                                      year=int(instance['scolaryear']), start=start, end=end, roadblock=False)
                    elements.append(elm.id)
                    db.connections.close_all()
                except:
                    pass
            nb_thread -= 1
            print("FINISH %s" % self.module.code_module)
        except:
            logging.error("{} {}".format(self.module, self.year))


def get_instances(intra):
    year = get_current_year()
    tasks = []
    for module in Module.objects.all():
        tasks.append(GetInstance(intra, module, year))
        if len(tasks) >= max_thread * 2:
            start_join_tasks(tasks)
            tasks = []
    start_join_tasks(tasks)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('authlink', type=str)

    def handle(self, *args, **options):
        if "authlink" not in options:
            raise CommandError('you must provide intra authlink')
        intra = IntraAPI(options['authlink'])
        get_instances(intra)
        Instance.objects.exclude(id__in=elements).delete()

