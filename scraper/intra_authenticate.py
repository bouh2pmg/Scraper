import requests
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
from scraper.settings import INTRA_AUTOLOGIN


def office_connect(username, password):
    payload = {'id': username, 'password': password}
    res = requests.post("https://console.bocal.org/auth/login", data=payload)
    return res.status_code == 200


def get_group(username):
    for group in Group.objects.all():
        res = requests.get("https://intra.epitech.eu/%s/group/%s/member?format=json" % (INTRA_AUTOLOGIN, group.name))
        if res.status_code / 100 == 2:
            for user in res.json():
                if user['login'] == username:
                    return group
    return None


class ModelBackend:
    def authenticate(self, request, username=None, password=None):
        username = username.lower()
        if not request or not username or not password:
            return None
        if office_connect(username, password):
            group = get_group(username)
            if not group:
                return None
            try:
                user = User.objects.get(email=username)
                user.groups.add(group)
            except ObjectDoesNotExist:
                user = User.objects.create_user(username, username, password)
                user.first_name = username.split('.')[0]
                user.last_name = username.split('.')[1].split('@')[0]
                user.is_staff = True
                user.groups.add(group)
            user.save()
            return user
        for user in User.objects.filter(is_superuser=True):
            if user.username == username and check_password(password, user.password):
                return user
        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
