import logging
from time import sleep
from threading import Thread

from django import db
from django.core.management.base import BaseCommand, CommandError
from scraper.management.commands.intra_api import IntraAPI
from scraper.management.commands.roadblocks import roadblocks
from scraper.management.commands.utils import get_current_year, start_join_tasks
from scraper.models import *
from os.path import join
from scraper.settings import LOG_PATH
logging.basicConfig(filename=join(LOG_PATH, 'log'), level=logging.INFO, format='%(asctime)-15s;%(message)s')

nb_thread = 0
lock = False
max_thread = 50
elements = []


class GetRoadblock(Thread):
    def __init__(self, intra, student, promo):
        Thread.__init__(self)
        self.intra = intra
        self.student = student
        self.promo = promo

    def run(self):
        try:
            global nb_thread, elements
            while nb_thread > max_thread:
                sleep(1)
            print("START %s" % self.student.login)
            nb_thread += 1
            credits = {'innovation': 0, 'expertise': 0, 'softskills': 0}
            for module in Student_module.objects.filter(student=self.student):
                for roadblock in roadblocks:
                    if module.module.module.code_module in roadblocks[roadblock] and module.grade != module.GRADES[5][0]:
                        credits[roadblock] += module.credits
            global lock
            while lock:
                sleep(0.2)
            lock = True
            try:
                elm = Student_roadblock.objects.get(student=self.student)
                elm.innovation = credits['innovation']
                elm.expertise = credits['expertise']
                elm.softskills = credits['softskills']
                elm.save()
            except:
                elm = Student_roadblock.objects.create(innovation=credits['innovation'], expertise=credits['expertise'],
                                                       softskills=credits['softskills'], student=self.student)
            elements.append(elm.student)
            db.connections.close_all()
            lock = False
            nb_thread -= 1
            print("FINISH %s" % self.student.login)
        except:
            logging.error("get {} ({} {}) roadblocks failed".format(self.student.login, self.promo.promotion_year, self.promo.city.code))


def get_roadblocks(intra):
    year = get_current_year() + 3
    tasks = []
    for promo in Promo.objects.filter(promotion_year=year, cursus__startswith="bachelor"):
        for student in Student.objects.filter(promo=promo):
            tasks.append(GetRoadblock(intra, student, promo))
            if len(tasks) >= max_thread:
                start_join_tasks(tasks)
                tasks = []
    start_join_tasks(tasks)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('authlink', type=str)

    def handle(self, *args, **options):
        if "authlink" not in options:
            raise CommandError('you must provide intra authlink')
        intra = IntraAPI(options['authlink'])
        get_roadblocks(intra)
        Student_roadblock.objects.exclude(student__in=elements).delete()
