#!/bin/bash

authlink=$1
shift
docker_compose=$(which docker-compose)
for cmd in "$@"
do    
    $docker_compose run django bash -c "echo -e \"import logging, os\nfrom scraper.settings import LOG_PATH; logging.basicConfig(filename='/src/log/log', level=logging.INFO, format='%(asctime)-15s;%(message)s')\nlogging.info('exec $cmd')\" | ./manage.py shell; ./manage.py $cmd $authlink"
done
