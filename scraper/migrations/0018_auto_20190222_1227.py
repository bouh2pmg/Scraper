# Generated by Django 2.1.5 on 2019-02-22 12:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraper', '0017_student_hors_parcours'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='student',
            name='hors_parcours',
        ),
        migrations.AlterField(
            model_name='tepitechscores',
            name='date',
            field=models.DateTimeField(null=True),
        ),
    ]
