from django.contrib import admin
from .models import *
from django.db.models import Sum


class CityAdmin(admin.ModelAdmin):
    list_display = ('code', 'name')


class PromoAdmin(admin.ModelAdmin):
    list_display = ('city', 'promotion_year', 'cursus')


class StudentAdmin(admin.ModelAdmin):
    @staticmethod
    def get_promo_year(obj):
        return obj.promo.promotion_year

    @staticmethod
    def get_promo_city(obj):
        return obj.promo.city.name

    @staticmethod
    def get_promo_cursus(obj):
        return obj.promo.cursus
    list_display = ('login', 'get_promo_year', 'get_promo_city', 'get_promo_cursus', 'credit_acquired')
    search_fields = ('promo__city__name',)


class ModuleAdmin(admin.ModelAdmin):
    list_display = ('code_module', 'name')


class InstanceAdmin(admin.ModelAdmin):
    list_display = ('module', 'code_instance', 'year', 'start', 'end', 'roadblock')


class Student_moduleAdmin(admin.ModelAdmin):
    @staticmethod
    def get_student(obj):
        return obj.student.login

    @staticmethod
    def get_module(obj):
        return obj.module.module.code_module
    @staticmethod
    def get_city(obj):
        return obj.student.promo.city.code
    list_display = ('get_student', 'get_city', 'get_module', 'credits', 'grade')


class Student_roadblockAdmin(admin.ModelAdmin):
    def student_login(self, obj):
        return obj.student.login
    def city(self, obj):
        return obj.student.promo.city.name
    def student_credits(self, obj):
        return Student_module.objects.filter(student=obj.student).aggregate(Sum('credits'))['credits__sum']
    list_display = ('student_login', 'city', 'innovation', 'expertise', 'softskills', 'student_credits')
    search_fields = ('student__promo__city__name',)


admin.site.register(City, CityAdmin)
admin.site.register(Promo, PromoAdmin)
admin.site.register(Module, ModuleAdmin)
admin.site.register(Instance, InstanceAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(Student_module, Student_moduleAdmin)
admin.site.register(Student_roadblock, Student_roadblockAdmin)
admin.site.register(GradeHistory)
admin.site.register(TepitechScores)

'''
admin.site.register(Weekly_log)
admin.site.register(Weekly_absences)
admin.site.register(Tepitech_scores)
'''
