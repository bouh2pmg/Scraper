import re
from django.utils import timezone


def get_promotion_year(promo, year):
    return year + 5 - int(re.search(r'\d+', promo).group()) + 1


def get_current_year():
    return timezone.now().year - (1 if timezone.now().month in range(1, 9) else 0)


def check_cursus(cursus):
    return "bachelor" in cursus or "master" in cursus or "PGT" in cursus


def start_join_tasks(tasks):
    for task in tasks:
        task.start()
    for task in tasks:
        task.join()
